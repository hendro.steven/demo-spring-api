package com.training.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.data.entities.Session;
import com.training.data.entities.User;
import com.training.data.repositories.SessionRepo;
import com.training.data.repositories.UserRepo;

import jakarta.transaction.Transactional;

@Service
@Transactional
public class UserService {

    @Autowired
    private UserRepo repo;

    @Autowired
    private SessionRepo sessionRepo;

    public Iterable<User> findAllUsers() {
        return repo.findAll();
    }

    public User findUserById(long id) {
        return repo.findById(id).orElse(null);
    }

    public void removeUserById(long id) {
        repo.deleteById(id);
    }

    public User findUserByEmail(String email) {
        return repo.findByEmail(email);
    }

    public void addUser(User user) {
        repo.save(user);
    }

    public void updateUser(User user) {
        repo.save(user);
    }

    public User findUserByEmailAndPassword(String email, String password) {
        return repo.findByEmailAndPassword(email, password);
    }

    public Session login(String email, String password) {
        User user = findUserByEmailAndPassword(email, password);

        if (user == null) {
            return null;
        }

        try {
            Session session = new Session();
            session.setUser(user);
            session.setActive(true);
            session.setSessionId(System.currentTimeMillis() + "" + user.getId());
            return sessionRepo.save(session);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean logout(String sessionId) {
        Session session = sessionRepo.findBySessionId(sessionId);
        if (session == null) {
            return false;
        }
        session.setActive(false);
        session.setLogoutAt(new Date());
        sessionRepo.save(session);
        return true;
    }
}
