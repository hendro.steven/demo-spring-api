package com.training.services;

import java.util.List;

import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.training.data.entities.Category;
import com.training.data.repositories.CategoryRepo;

import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@Service
@Transactional
public class CategoryService {

    @Autowired
    private CategoryRepo repo;

    @Autowired
    private EntityManager entityManager;

    public Iterable<Category> findAllCategories(boolean isHapus) {
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedCategoryFilter");
        filter.setParameter("isDeleted", isHapus);
        Iterable<Category> categories = repo.findAll();
        session.disableFilter("deletedCategoryFilter");
        return categories;
        // return repo.findByDeleted(isHapus);
    }

    public Category findCategoryById(long id) {
        return repo.findById(id).orElse(null);
    }

    public boolean removeCategoryById(long id) {
        try {
            Category category = findCategoryById(id);
            if (category == null) {
                return false;
            }
            repo.delete(category);
            return true;
        } catch (Exception e) {
            throw new RuntimeException("Something went wrong. Please try again later.");
        }

    }

    public Category addCategory(Category category) {
        return repo.save(category);
    }

    public Category updateCategory(Category category) {
        return repo.save(category);
    }

    public Category findByName(String name) {
        return repo.cariBerdasarkanNama(name);
    }

    public List<Category> findByNameContaining(String name) {
        return repo.findByNameContaining(name);
    }
}
