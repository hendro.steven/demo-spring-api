package com.training.dto;

import lombok.Data;

@Data
public class RequestSearchCategory {
    private String searchKey;
}
