package com.training.dto;

import com.training.validators.UniqueCodeConstrain;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class RequestProduct {

    @NotEmpty(message = "Code is required")
    @Size(min = 3, max = 5, message = "Code must be between 3 and 5 characters")
    @Pattern(regexp = "P[0-9]+", message = "Code must start with P followed by numbers")
    @UniqueCodeConstrain
    private String code;

    @NotEmpty(message = "Name is required")
    private String name;

    @NotNull(message = "Price is required")
    private double price;

    @NotEmpty(message = "Description is required")
    private String description;

    @NotNull(message = "Category is required")
    private long categoryId;
}
