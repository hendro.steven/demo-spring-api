package com.training.dto;

import com.training.validators.PasswordEqualConstrain;
import com.training.validators.UniqueEmailConstrain;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

@Data
@PasswordEqualConstrain(message = "Password dan RetypePassword harus sama")
public class RequestRegister {

    @NotEmpty(message = "Email is required")
    @Pattern(regexp = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$", message = "Email is not valid")
    @UniqueEmailConstrain
    private String email;

    @NotEmpty(message = "Password is required")
    private String password;

    @NotEmpty(message = "ReTypePassword is required")
    private String retypePassword;

    @NotEmpty(message = "First name is required")
    private String firstName;

    @NotEmpty(message = "Last name is required")
    private String lastName;

    private int age;
}
