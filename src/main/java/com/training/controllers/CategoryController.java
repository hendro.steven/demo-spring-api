package com.training.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.data.entities.Category;
import com.training.dto.RequestSearchCategory;
import com.training.dto.ResponseData;
import com.training.services.CategoryService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryController {

    @Autowired
    private CategoryService service;

    @GetMapping("/deleted/{isHapus}")
    public ResponseEntity<ResponseData<Iterable<Category>>> findAllCategories(
            @PathVariable("isHapus") boolean isHapus) {
        ResponseData<Iterable<Category>> response = new ResponseData<>();
        try {
            System.out.println("isHapus: " + isHapus);
            response.setPayload(service.findAllCategories(isHapus));
            response.setStatus(true);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<Category>> findCategoryById(@PathVariable("id") long id) {
        ResponseData<Category> response = new ResponseData<>();
        try {
            Category category = service.findCategoryById(id);
            if (category == null) {
                response.getMessages().add("Category not found");
                response.setStatus(false);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            response.setPayload(category);
            response.setStatus(true);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> removeCategoryById(@PathVariable("id") long id) {
        ResponseData response = new ResponseData();
        try {
            boolean result = service.removeCategoryById(id);
            if (!result) {
                response.getMessages().add("Category not found");
                response.setStatus(result);
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
            }
            response.setPayload(result);
            response.setStatus(result);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @PostMapping
    public ResponseEntity<?> saveCategory(@Valid @RequestBody Category category, Errors errors) {
        ResponseData response = new ResponseData();

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> response.getMessages().add(error.getDefaultMessage()));
            response.setStatus(false);
            return ResponseEntity.badRequest().body(response);
        }

        try {
            response.setPayload(service.addCategory(category));
            response.setStatus(true);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @PutMapping
    public ResponseEntity<?> updateCategory(@RequestBody Category category) {
        ResponseData response = new ResponseData();
        try {
            response.setPayload(service.updateCategory(category));
            response.setStatus(true);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @PostMapping("/search")
    public ResponseEntity<?> findCategoryByName(@RequestBody RequestSearchCategory request) {
        ResponseData response = new ResponseData();
        try {
            response.setPayload(service.findByName(request.getSearchKey()));
            response.setStatus(true);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @PostMapping("/search/like")
    public ResponseEntity<?> findCategoryByNameContaining(@RequestBody RequestSearchCategory request) {
        ResponseData response = new ResponseData();
        try {
            response.setPayload(service.findByNameContaining(request.getSearchKey()));
            response.setStatus(true);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
