package com.training.controllers;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.data.entities.User;
import com.training.dto.RequestRegister;
import com.training.dto.ResponseData;
import com.training.dto.ResponseRegister;
import com.training.services.UserService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/users/register")
public class RegisterUserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @PostMapping
    public ResponseEntity<ResponseData<ResponseRegister>> registerUser(
            @Valid @RequestBody RequestRegister requestRegister, Errors errors) {
        ResponseData<ResponseRegister> response = new ResponseData<>();

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> {
                response.getMessages().add(error.getDefaultMessage());
            });
            response.setStatus(false);
            return ResponseEntity.badRequest().body(response);
        }

        try {
            User user = modelMapper.map(requestRegister, User.class);
            userService.addUser(user);

            response.setStatus(true);
            response.getMessages().add("User registered successfully");
            response.setPayload(modelMapper.map(user, ResponseRegister.class));
            return ResponseEntity.ok(response);

        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add("Error: " + e.getMessage());
            return ResponseEntity.status(500).body(response);
        }
    }

}
