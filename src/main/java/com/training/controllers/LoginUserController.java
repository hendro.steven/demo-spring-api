package com.training.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.dto.RequestLogin;
import com.training.dto.ResponseData;
import com.training.services.UserService;

@RestController
@RequestMapping("/api/v1/users/login")
public class LoginUserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity<?> login(@RequestBody RequestLogin login) {
        ResponseData response = new ResponseData();
        try {
            response.setPayload(userService.login(login.getEmail(), login.getPassword()));
            response.setStatus(true);
            response.getMessages().add("Login successful");
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setStatus(false);
            response.getMessages().add("Login failed: " + e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
