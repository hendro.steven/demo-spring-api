package com.training.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.data.entities.Product;
import com.training.dto.RequestProduct;
import com.training.dto.ResponseData;
import com.training.services.CategoryService;
import com.training.services.ProductService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    private ProductService service;

    @Autowired
    private CategoryService categoryService;

    @PostMapping
    public ResponseEntity<?> createProduct(@Valid @RequestBody RequestProduct request, Errors errors) {
        ResponseData response = new ResponseData();

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> response.getMessages().add(error.getDefaultMessage()));
            response.setStatus(false);
            return ResponseEntity.badRequest().body(response);
        }

        try {
            Product product = new Product();
            product.setCode(request.getCode());
            product.setName(request.getName());
            product.setPrice(request.getPrice());
            product.setDescription(request.getDescription());
            product.setCategory(categoryService.findCategoryById(request.getCategoryId()));

            response.setPayload(service.addProduct(product));
            response.setStatus(true);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/{size}/{page}")
    public ResponseEntity<?> getAllProduct(@PathVariable("size") int size, @PathVariable("page") int page) {
        ResponseData response = new ResponseData();
        try {
            response.setPayload(service.findAllProducts(size, page));
            response.setStatus(true);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }

    @GetMapping("/search/category/{id}")
    public ResponseEntity<?> getProductsByCategory(@PathVariable("id") long categoryId) {
        ResponseData response = new ResponseData();
        try {
            response.setPayload(service.searchProductByCategory(categoryId));
            response.setStatus(true);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.getMessages().add(e.getMessage());
            response.setStatus(false);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
        }
    }
}
