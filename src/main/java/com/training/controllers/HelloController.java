package com.training.controllers;

import java.util.HashMap;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HelloController {

    @GetMapping
    public Object sayHello() {
        HashMap<String, String> map = new HashMap<>();
        map.put("message", "Hello, World!");
        map.put("status", "success");
        return map;
    }
}
