package com.training.validators;

import com.training.dto.RequestRegister;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PasswordEqualValidator implements ConstraintValidator<PasswordEqualConstrain, Object> {

    @Override
    public boolean isValid(Object data, ConstraintValidatorContext context) {
        RequestRegister requestRegister = (RequestRegister) data;
        return requestRegister.getPassword().equals(requestRegister.getRetypePassword());
    }

}
