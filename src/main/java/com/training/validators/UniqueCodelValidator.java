package com.training.validators;

import org.springframework.beans.factory.annotation.Autowired;

import com.training.data.repositories.ProductRepo;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class UniqueCodelValidator implements ConstraintValidator<UniqueCodeConstrain, String> {

    @Autowired
    private ProductRepo productRepo;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        // cek apapun disini
        return !productRepo.existsByCode(value);
    }

}
