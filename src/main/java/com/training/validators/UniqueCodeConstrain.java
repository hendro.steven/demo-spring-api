package com.training.validators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;

@Target({ ElementType.FIELD, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UniqueCodelValidator.class)
public @interface UniqueCodeConstrain {

    String message() default "Code is already registered";

    Class<?>[] groups() default {};

    Class<?>[] payload() default {};
}
