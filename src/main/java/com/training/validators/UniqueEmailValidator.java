package com.training.validators;

import org.springframework.beans.factory.annotation.Autowired;

import com.training.data.repositories.UserRepo;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class UniqueEmailValidator implements ConstraintValidator<UniqueEmailConstrain, String> {

    @Autowired
    private UserRepo userRepo;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return !userRepo.existsByEmail(value);
    }

}
