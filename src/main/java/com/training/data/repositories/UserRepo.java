package com.training.data.repositories;

import org.springframework.data.repository.CrudRepository;

import com.training.data.entities.User;

public interface UserRepo extends CrudRepository<User, Long> {

    public User findByEmailAndPassword(String email, String password);

    public User findByEmail(String email);

    public boolean existsByEmail(String email);
}
