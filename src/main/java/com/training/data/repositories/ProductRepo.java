package com.training.data.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.training.data.entities.Category;
import com.training.data.entities.Product;

public interface ProductRepo extends PagingAndSortingRepository<Product, Long>, CrudRepository<Product, Long> {

    public List<Product> findByCategory(Category category);

    public List<Product> findByCategoryId(long categoryId);

    public List<Product> findByNameContaining(String name);

    public List<Product> findByPriceBetween(double min, double max);

    public List<Product> findByNameContainingAndPriceBetween(String name, double min, double max);

    public boolean existsByCode(String code);
}
