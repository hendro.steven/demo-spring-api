package com.training.data.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.training.data.entities.Category;

public interface CategoryRepo extends CrudRepository<Category, Long> {

    @Query("SELECT c FROM Category c WHERE c.name = :paramName")
    public Category cariBerdasarkanNama(@Param("paramName") String name); // used jpql

    public Category findByName(String name); // used naming convention or derived name query

    public List<Category> findByNameContaining(String name); // used naming convention or derived name query

    public List<Category> findByDeleted(boolean deleted); // used naming convention or derived name query
}
