package com.training.data.repositories;

import org.springframework.data.repository.CrudRepository;

import com.training.data.entities.Session;

public interface SessionRepo extends CrudRepository<Session, Long> {

    public Session findBySessionId(String sessionId);
}
